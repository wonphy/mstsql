USE [pit_to_port_pnc]
GO
/****** Object:  UserDefinedFunction [dbo].[uf_get_index_table_included_scope]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		Fei Wang
-- Create date: 16/12/2013, 17/12/2013
-- Description:	This function calculate the table scoping statement, for those tables that are not primary key tables, 
-- add a '?' + TABLENAME in uppercase + '?' to the statement, which can be used to decide ignore this table or not.
CREATE FUNCTION [dbo].[uf_get_index_table_included_scope]  
(
	-- Add the parameters for the function here 
	@p_index_table_name		varchar(128),
	@p_index_column_name	varchar(128),
	@p_table_name			varchar(128)
)
RETURNS varchar(max)
WITH EXECUTE AS OWNER 
AS
BEGIN
	declare @pk_table_name varchar(max), @pk_column_name varchar(128), @fk_column_name varchar(128), @fk_column_nullable int, @reference_scope varchar(max)
	declare @table_scope_r_idx varchar(max), @table_scope_r varchar(max)
	
	-- declare a cursor for getting all primary key tables of the given table from dbo.sys_index_table_included_scope.
	declare csr_scope cursor local read_only for 
		select a.pk_table_name, a.pk_column_name, a.fk_column_name, a.fk_column_nullable, a.reference_scope  
		from dbo.sys_index_table_included_scope a
		where upper(a.index_table_name) = upper(@p_index_table_name) 
			and upper(a.fk_table_name) = upper(@p_table_name) 
			and (upper(a.pk_table_name) <> upper(a.fk_table_name) or upper(a.pk_table_name) = upper(a.index_table_name)) 
		order by a.reference_depth
	
	-- open cursor, start to calculate the scoping statement.
	select @table_scope_r = '1 = 1'
	open csr_scope
	fetch next from csr_scope  
	into @pk_table_name, @pk_column_name, @fk_column_name, @fk_column_nullable, @reference_scope 
	while @@fetch_status = 0
	begin 
		-- if the given table has a primary key table which is exact the index table, and the foreign column is not nullable, then ignore other reference scopes.
		if upper(@pk_table_name) = upper(@p_index_table_name) and upper(@pk_column_name) = upper(@p_index_column_name) and upper(@p_table_name) <> upper(@p_index_table_name) 
		begin
			--if @fk_column_nullable = 0 
			--begin
				if @table_scope_r_idx is null 
					select @table_scope_r_idx = @reference_scope
				else
					select @table_scope_r_idx = @table_scope_r_idx + ' or (' + @reference_scope + ')'
			--end
		end
		
		if @table_scope_r_idx is null
		begin 
			-- combine all reference scope statement.
			if @fk_column_nullable = 0 
			begin
				select @table_scope_r = '(' + @table_scope_r + ') and (' + @reference_scope + ')'
				break
			end
			else if @fk_column_nullable = 1 
				select @table_scope_r = '(' + @table_scope_r + ') or (' + @reference_scope + ')'
		end
		
		fetch next from csr_scope 
		into @pk_table_name, @pk_column_name, @fk_column_name, @fk_column_nullable, @reference_scope
	end
	close csr_scope
	deallocate csr_scope
	
	if @table_scope_r_idx is not null
		select @table_scope_r = @table_scope_r_idx
	else 
	begin
		select @table_scope_r = replace(@table_scope_r, '(1 = 1) and ', '')
		select @table_scope_r = replace(@table_scope_r, '(1 = 1) or ', '')
	end
		
	-- if the given table has no foreign key tables, then add a '?' + TABLENAME in uppercase + '?' to the scoping statement.
	if not exists (select 1 from dbo.sys_index_table_included_scope where upper(pk_table_name) = upper(@p_table_name)) 
		select @table_scope_r = '(?' + upper(@p_table_name) + '?) and (' + @table_scope_r + ')'
	
	return @table_scope_r

END
GO
/****** Object:  UserDefinedFunction [dbo].[uf_trip_to_xml]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[uf_trip_to_xml] 
(
	-- Add the parameters for the function here
	@iscenario_id		int,
	@trip_id			int
)
RETURNS varchar(max)
AS
BEGIN
	declare @xml varchar(max)
	declare @tripleg_id int
	declare @tripleg_xml varchar(max)
	declare @trippoint_xml varchar(max)
		
	select @xml = '<trainTrip action="CREATE" syncId="123"><trainTripCode>' + a.string_field1 + '</trainTripCode><regionCode>' + b.code + '</regionCode><trainConsistCode>' + e.code + '</trainConsistCode>'
	from activities a, resource_area b, scenario_resource_activity_join_table c, scenario_resource d, resource e 
	where a.integrated_scenario_id = @iscenario_id
		and a.type_id = 2
		and a.id = @trip_id
		and b.id = a.location
		and c.activity_id = a.id
		and d.id = c.scenario_resource_id
		and e.id = d.resource_id
		
	select @xml = @xml + '<tripLegs>'
	
	declare c_triplegs cursor local read_only for 
		select a.id, '<tripLeg><tripLegCode>' + a.string_field1 + '</tripLegCode><tripLegType>' + 
			(case a.type_id when 3 then 'FORWARD' when 4 then 'RETURN' when 5 then 'TRANSFER' end) + '</tripLegType>'
		from activities a
		where a.integrated_scenario_id = @iscenario_id
			and a.parent_id = @trip_id
			and a.type_id in (3, 4, 5)
		order by a.start_date
		
	open c_triplegs
	fetch next from c_triplegs 
	into @tripleg_id, @tripleg_xml
	while @@fetch_status = 0
	begin
		select @xml = @xml + @tripleg_xml
		select @xml = @xml + '<tripPoints>'
		
		declare c_trippoints cursor local read_only for 
		select '<tripPoint><pointSequence>' + cast(a.sequence as varchar(50)) + '</pointSequence><pointType>' + 
			(case a.type_id when 7 then 'DWELL' when 8 then 'LOAD' when 9 then 'UNLOAD' end) + '</pointType><locationCode>' + d.code + '</locationCode><plannedTime>'
			+ '<arrival>' + convert(char(19), a.start_date, 126) + '</arrival><departure>' + convert(char(19), a.end_date, 126) + '</departure></plannedTime></tripPoint>' 
		from activities a, scenario_resource_activity_join_table b, scenario_resource c, resource d 
		where a.integrated_scenario_id = @iscenario_id
			and a.parent_id = @tripleg_id
			and a.type_id in (7, 8, 9) 
			and b.activity_id = a.id
			and c.id = b.scenario_resource_id
			and d.id = c.resource_id
			and d.resource_type in ('1007503', '1007504', '1007505', '1007506', '1007508')
		order by a.start_date
		open c_trippoints
		
		fetch next from c_trippoints 
		into @trippoint_xml
		while @@fetch_status = 0
		begin
			select @xml = @xml + @trippoint_xml
			
			fetch next from c_trippoints 
			into @trippoint_xml
		end
		close c_trippoints
		deallocate c_trippoints
		
		select @xml = @xml + '</tripPoints>'
		select @xml = @xml + '</tripLeg>'
		
		fetch next from c_triplegs 
		into @tripleg_id, @tripleg_xml
	end
	close c_triplegs
	deallocate c_triplegs
	select @xml = @xml + '</tripLegs>'
	select @xml = @xml + '</trainTrip>'
	
	return @xml

END
GO
/****** Object:  Table [dbo].[sys_index_table_included_clean]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_index_table_included_clean](
	[database_version] [varchar](50) NOT NULL,
	[clean_batch_code] [varchar](50) NOT NULL,
	[pk_table_name] [varchar](128) NOT NULL,
	[pk_column_name] [varchar](128) NOT NULL,
	[fk_table_name] [varchar](128) NOT NULL,
	[fk_column_name] [varchar](128) NOT NULL,
	[fk_table_order] [int] NOT NULL,
	[fk_table_scope] [varchar](max) NOT NULL,
	[cleaned_rows] [bigint] NULL,
	[cleaned_begintime] [datetime] NULL,
	[cleaned_endtime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_index_table_included_copy]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_index_table_included_copy](
	[database_version] [varchar](50) NOT NULL,
	[copy_batch_code] [varchar](50) NOT NULL,
	[pk_table_name] [varchar](128) NOT NULL,
	[pk_column_name] [varchar](128) NOT NULL,
	[fk_table_name] [varchar](128) NOT NULL,
	[fk_column_name] [varchar](128) NOT NULL,
	[fk_column_nullable] [int] NOT NULL,
	[fk_table_order] [int] NOT NULL,
	[fk_table_scope] [varchar](max) NOT NULL,
	[fk_table_columns] [varchar](max) NOT NULL,
	[copied_startid] [bigint] NULL,
	[copied_rows] [bigint] NULL,
	[copied_begintime] [datetime] NULL,
	[copied_endtime] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[sys_index_table_included_scope]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[sys_index_table_included_scope](
	[database_version] [varchar](50) NOT NULL,
	[index_table_name] [varchar](128) NOT NULL,
	[pk_table_name] [varchar](128) NOT NULL,
	[pk_column_name] [varchar](128) NOT NULL,
	[fk_table_name] [varchar](128) NOT NULL,
	[fk_column_name] [varchar](128) NOT NULL,
	[fk_column_ordinal] [int] NOT NULL,
	[fk_column_nullable] [int] NOT NULL,
	[reference_depth] [int] NOT NULL,
	[reference_scope] [varchar](max) NOT NULL,
	[fk_table_order] [int] NULL,
	[fk_table_scope] [varchar](max) NULL,
	[fk_table_columns] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[v_table_columns]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_table_columns] 
as 
select table_name, column_name, ordinal_position, columnproperty(object_id(table_name), column_name, 'IsIdentity') is_identity, is_nullable   
from information_schema.columns 
GO
/****** Object:  View [dbo].[v_table_primarykeys]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[v_table_primarykeys] as 
select a.table_name, b.column_name
from information_schema.table_constraints a 
inner join (
	select x.constraint_catalog, x.constraint_schema, x.constraint_name, x.column_name 
	from information_schema.key_column_usage x
	where not exists (
		select constraint_catalog, constraint_schema, constraint_name 
		from information_schema.key_column_usage 
		where constraint_catalog = x.constraint_catalog 
			and constraint_schema = x.constraint_schema
			and constraint_name = x.constraint_name 
		group by constraint_catalog, constraint_schema, constraint_name 
		having count(1) > 1
		)
	) b 
on b.constraint_catalog = a.constraint_catalog  
    and b.constraint_schema = a.constraint_schema 
    and b.constraint_name = a.constraint_name 
where a.constraint_type = 'PRIMARY KEY' 
	and a.table_schema = 'dbo' 
	and columnproperty(object_id(a.table_name), b.column_name, 'IsIdentity') = 1

GO
/****** Object:  View [dbo].[v_table_references]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[v_table_references] as 
select distinct x.pk_table_name, x.pk_column_name, x.fk_table_name, x.fk_column_name, y.ordinal_position fk_column_ordinal, 
case y.is_nullable when 'YES' then 1 else 0 end fk_column_nullable  
from (
select refc.constraint_catalog table_catalog
	,refc.constraint_schema table_schema
	,keycupk.table_name pk_table_name 
    ,keycupk.column_name pk_column_name
    ,keycufk.table_name fk_table_name 
    ,keycufk.column_name fk_column_name
from (
	select a.constraint_catalog, a.constraint_schema, a.constraint_name, a.unique_constraint_catalog, 
		a.unique_constraint_schema, a.unique_constraint_name 
	from information_schema.referential_constraints a 
	inner join information_schema.table_constraints b 
	on b.constraint_catalog = a.constraint_catalog 
		and b.constraint_schema = a.constraint_schema 
		and b.constraint_name = a.unique_constraint_name 
		and b.constraint_type = 'PRIMARY KEY' 
		and b.table_schema = 'dbo' 
	) refc
inner join information_schema.key_column_usage keycufk 
    on keycufk.constraint_catalog = refc.constraint_catalog  
    and keycufk.constraint_schema = refc.constraint_schema 
    and keycufk.constraint_name = refc.constraint_name 
inner join information_schema.key_column_usage keycupk 
    on keycupk.constraint_catalog = refc.unique_constraint_catalog  
    and keycupk.constraint_schema = refc.unique_constraint_schema 
    and keycupk.constraint_name = refc.unique_constraint_name 
    and keycupk.ordinal_position = keycufk.ordinal_position
    ) x 
inner join information_schema.columns y 
	on y.table_catalog = x.table_catalog 
	and y.table_schema = x.table_schema 
	and y.table_name = x.fk_table_name
	and y.column_name = x.fk_column_name 	

GO
/****** Object:  StoredProcedure [dbo].[usp_bulkcleaner]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_bulkcleaner]
	-- Add the parameters for the stored procedure here 
	@p_index_table_name		varchar(128),
	@p_index_cleanidscope	varchar(max),
	@p_clean_batchcode		varchar(50),
	@p_excluded_tablenames	varchar(max), 
	@p_clean_to_index_table	int = 0
WITH EXECUTE AS OWNER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	declare @p_index_column_name varchar(128)
	declare @p_excluded_tablenames_x varchar(max)
	
	select * 
	into #table_columns 
	from dbo.v_table_columns
	
	select top 1 @p_index_column_name = column_name 
	from #table_columns 
	where upper(table_name) = upper(@p_index_table_name) 
		and is_identity = 1

	if @p_index_column_name = null
		raiserror ('Sorry, table %1 has no single identity primary key.', 42301, 1, @p_index_table_name)	
	
	exec dbo.usp_index_table_included_scope_setup @p_index_table_name, @p_index_column_name
	
	delete from dbo.sys_index_table_included_clean where clean_batch_code = @p_clean_batchcode
	
	insert into dbo.sys_index_table_included_clean
	select database_version, @p_clean_batchcode, pk_table_name, pk_column_name, fk_table_name, fk_column_name, fk_table_order, fk_table_scope, null, null, null
	from dbo.sys_index_table_included_scope 
	where upper(index_table_name) = upper(@p_index_table_name) 
		and fk_table_order is not null
	
	update dbo.sys_index_table_included_clean 
	set fk_table_scope = replace(fk_table_scope, '#ID#', @p_index_cleanidscope)
	where clean_batch_code = @p_clean_batchcode
	
	exec dbo.usp_bulkcleaner_clean @p_clean_batchcode, @p_excluded_tablenames, @p_clean_to_index_table
	
	drop table #table_columns
	
	delete from dbo.sys_index_table_included_clean 
	where datediff(day, cleaned_begintime, getdate()) > 1
END
GO
/****** Object:  StoredProcedure [dbo].[usp_bulkcleaner_clean]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_bulkcleaner_clean]
	-- Add the parameters for the stored procedure here 
	@p_clean_batchcode		varchar(50), 
	@p_excluded_tablenames  varchar(max), 
	@p_clean_to_index_table	int
WITH EXECUTE AS OWNER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON	
	
	declare @pk_table_name varchar(128)
	declare @pk_column_name varchar(128)
	declare @p_table_name varchar(128)
	declare @fk_column_name varchar(128)
	declare @table_scope nvarchar(max)
	declare @sql nvarchar(max)
	declare @cleaned_rows1 bigint
	declare @cleaned_rows2 bigint
	
	declare @excluded_tablename varchar(150)
	
	declare @table_pk_columnname varchar(150)
	declare @min_pk_columnvalue bigint
	declare @max_pk_columnvalue bigint
	
	declare @table_order int
	while exists(select top 1 a.fk_table_name, a.fk_table_order, a.fk_table_scope 
		from dbo.sys_index_table_included_clean a 
		where a.clean_batch_code = @p_clean_batchcode and a.cleaned_rows is null
			and (a.fk_table_order > 0 or @p_clean_to_index_table = 1))
	begin
	
	select top 1 @pk_column_name=a.pk_column_name, @fk_column_name=a.fk_column_name, @pk_table_name=a.pk_table_name, @p_table_name=a.fk_table_name, @table_order=a.fk_table_order, @table_scope=a.fk_table_scope
		from dbo.sys_index_table_included_clean a 
		where a.clean_batch_code = @p_clean_batchcode and a.cleaned_rows is null and (a.fk_table_order > 0 or @p_clean_to_index_table = 1) order by a.fk_table_order desc
			
		while PATINDEX('%(?%?)%', @table_scope) > 0
		begin
			select @excluded_tablename = substring(@table_scope, PATINDEX('%(?%?)%', @table_scope), PATINDEX('%?)%', @table_scope) - PATINDEX('%(?%?)%', @table_scope) + 2)
			if charindex(@excluded_tablename, @p_excluded_tablenames) > 0 
				select @table_scope = replace(@table_scope, @excluded_tablename, '(0 = 1)')
			else
				select @table_scope = replace(@table_scope, @excluded_tablename + ' and', '')
		end
		update dbo.sys_index_table_included_clean 
		set fk_table_scope = @table_scope, cleaned_begintime = getdate()  
		where upper(fk_table_name) = upper(@p_table_name)
		
		declare @r_table_scope nvarchar(max) 
		select @r_table_scope = @table_scope
		if upper(@p_table_name) <> upper('integrated_scenario')

			exec dbo.usp_optimise_table_scope @p_table_name, @table_scope, @r_table_scope output 
		
		if upper(@pk_table_name) = upper(@p_table_name)
		begin
			select @sql = N'select @cleaned_rows1 = 0; '
			select @sql = @sql + 'delete from ' + @p_table_name + ' where ' + @fk_column_name + ' in (select ' + @pk_column_name + ' from ' + @p_table_name + ' where ' + @r_table_scope + ');'
			select @sql = @sql + 'select @cleaned_rows1 = @@rowcount;'
			print @sql
			exec sp_executesql @sql, N'@cleaned_rows1 bigint output', @cleaned_rows1 output
		end
		select @sql = N'select @cleaned_rows2 = 0; '
		select @sql = @sql + 'delete from ' + @p_table_name + ' where ' + @r_table_scope + ';'
		select @sql = @sql + 'select @cleaned_rows2 = @@rowcount;'
		print @sql
		exec sp_executesql @sql, N'@cleaned_rows2 bigint output', @cleaned_rows2 output
		
		update dbo.sys_index_table_included_clean 
		set cleaned_rows = isnull(@cleaned_rows1, 0) + @cleaned_rows2, cleaned_endtime = getdate() 
		where clean_batch_code = @p_clean_batchcode 
			and upper(fk_table_name) = upper(@p_table_name)
	end

END
GO
/****** Object:  StoredProcedure [dbo].[usp_bulkcopier]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_bulkcopier]
	-- Add the parameters for the stored procedure here 
	@p_index_table_name		varchar(128),
	@p_index_copyfromid		bigint,
	@p_index_copytoid		bigint,
	@p_copy_batchcode		varchar(50),
	@p_excluded_tablenames	varchar(max) = ''
WITH EXECUTE AS OWNER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	declare @p_index_column_name varchar(128)
	declare @p_excluded_tablenames_x varchar(max)
	
	select * 
	into #table_columns 
	from dbo.v_table_columns
	
	select top 1 @p_index_column_name = column_name 
	from #table_columns 
	where upper(table_name) = upper(@p_index_table_name) 
		and is_identity = 1

	if @p_index_column_name = null
		raiserror ('Sorry, table %1 has no single identity primary key.', 42301, 1, @p_index_table_name)	
	
	exec dbo.usp_index_table_included_scope_setup @p_index_table_name, @p_index_column_name
	
	delete from dbo.sys_index_table_included_copy where copy_batch_code = @p_copy_batchcode
	
	insert into dbo.sys_index_table_included_copy
	select database_version, @p_copy_batchcode, pk_table_name, pk_column_name, fk_table_name, fk_column_name, fk_column_nullable, fk_table_order, fk_table_scope, 
		fk_table_columns, 
		case when upper(fk_table_name) = upper(@p_index_table_name) then @p_index_copytoid else null end, 
		case when upper(fk_table_name) = upper(@p_index_table_name) then 1 else null end, 
		case when upper(fk_table_name) = upper(@p_index_table_name) then getdate() else null end, 
		case when upper(fk_table_name) = upper(@p_index_table_name) then getdate() else null end
	from dbo.sys_index_table_included_scope 
	where upper(index_table_name) = upper(@p_index_table_name)
		and fk_table_order is not null
	
	update dbo.sys_index_table_included_copy 
	set fk_table_scope = replace(fk_table_scope, '#ID#', cast(@p_index_copyfromid as varchar(50)))
	where copy_batch_code = @p_copy_batchcode
	
	select @p_excluded_tablenames_x = '(?' + upper(replace(@p_excluded_tablenames, ',', '?),(?')) + '?)'
	
	exec dbo.usp_bulkcopier_copy @p_copy_batchcode, @p_excluded_tablenames_x, @p_index_table_name
	
	drop table #table_columns
	
	delete from dbo.sys_index_table_included_copy 
	where datediff(day, copied_begintime, getdate()) > 1
END
GO
/****** Object:  StoredProcedure [dbo].[usp_bulkcopier_copy]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_bulkcopier_copy]
	-- Add the parameters for the stored procedure here 
	@p_copy_batchcode		varchar(50), 
	@p_excluded_tablenames  varchar(max), 
	@p_index_table_name		varchar(128)
WITH EXECUTE AS OWNER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON		
	
	declare @excluded_tablename varchar(150)
	declare @p_table_name varchar(128)
	declare @p_pk_column_name varchar(128)
	declare @table_scope varchar(max)
	declare @p_table_columns varchar(max)
	declare @table_columns varchar(max)
	declare @sql nvarchar(max)
	declare @sql_join nvarchar(max)
	declare @rc bigint
	declare @copied_startid bigint
	declare @pk_table_name varchar(128)
	declare @pk_column_name varchar(128)
	declare @fk_column_name varchar(128)
	declare @fk_column_nullable int
	declare @pk_copied_startid bigint
	declare @pk_table_scope varchar(max)
	declare @upref_order int
		
	declare @table_order int
	declare copy_cursor cursor local read_only for 
		select distinct a.fk_table_name, a.fk_table_order, a.fk_table_scope, a.fk_table_columns  
		from dbo.sys_index_table_included_copy a 
		where a.copy_batch_code = @p_copy_batchcode 
			and a.copied_rows is null 
		order by a.fk_table_order asc, a.fk_table_name asc
	
	open copy_cursor
	fetch next from copy_cursor 
	into @p_table_name, @table_order, @table_scope, @table_columns
	while @@fetch_status = 0
	begin 
		select @p_table_columns = @table_columns
		
		while PATINDEX('%(?%?)%', @table_scope) > 0
		begin
			select @excluded_tablename = substring(@table_scope, PATINDEX('%(?%?)%', @table_scope), PATINDEX('%?)%', @table_scope) - PATINDEX('%(?%?)%', @table_scope) + 2)
			if charindex(@excluded_tablename, @p_excluded_tablenames) > 0 
				select @table_scope = replace(@table_scope, @excluded_tablename, '(0 = 1)')
			else
				select @table_scope = replace(@table_scope, @excluded_tablename + ' and', '')
		end
		update dbo.sys_index_table_included_copy 
		set fk_table_scope = @table_scope, copied_begintime = getdate()  
		where copy_batch_code = @p_copy_batchcode 
			and upper(fk_table_name) = upper(@p_table_name)			
		
		select @p_pk_column_name = ''
		select top 1 @p_pk_column_name = column_name 
		from #table_columns  
		where upper(table_name) = upper(@p_table_name)
			and is_identity = 1
		
		select @upref_order = 0
		select @sql_join = ''		
	
		declare upref_cursor cursor local read_only for 
		select a.pk_table_name, a.pk_column_name, a.fk_column_name, a.fk_column_nullable  
		from dbo.sys_index_table_included_copy a 
		where a.copy_batch_code = @p_copy_batchcode 
			and upper(a.fk_table_name) = upper(@p_table_name) 
			and upper(a.pk_table_name) <> upper(a.fk_table_name)
			
		open upref_cursor 
		fetch next from upref_cursor 
		into @pk_table_name, @pk_column_name, @fk_column_name, @fk_column_nullable
		while @@fetch_status = 0 
		begin			
			select top 1 @pk_copied_startid = a.copied_startid, @pk_table_scope = a.fk_table_scope  
			from dbo.sys_index_table_included_copy a 
			where a.copy_batch_code = @p_copy_batchcode 
				and upper(a.fk_table_name) = upper(@pk_table_name)
			
			if upper(@pk_table_name) = upper(@p_index_table_name) 
				select @table_columns = replace(@table_columns, 'x.[' + @fk_column_name + ']', cast(@pk_copied_startid as varchar(50)) + ' ' + @fk_column_name)
			else 
			begin
				select @upref_order = @upref_order + 1
				select @table_columns = replace(@table_columns, 'x.[' + @fk_column_name + ']', 
					'isnull(' + cast(@pk_copied_startid as varchar(50)) + ' + a' + cast(@upref_order as varchar(50)) + '.gap, ' + case when @fk_column_nullable = 0 then '-1' else 'null' end + ') ' + @fk_column_name)
				
				select @sql_join += ' left join (select ' + @pk_column_name + ', row_number() over (order by ' + @pk_column_name + ') - 1 gap ' 
					+ ' from ' + @pk_table_name + ' where ' + @pk_table_scope + ') a' 
					+ cast(@upref_order as varchar(50)) + ' on x.' + @fk_column_name + ' = a' + cast(@upref_order as varchar(50)) + '.' + @pk_column_name
			end
			
			fetch next from upref_cursor 
			into @pk_table_name, @pk_column_name, @fk_column_name, @fk_column_nullable
		end
		close upref_cursor
		deallocate upref_cursor
		
		if @p_pk_column_name <> ''
		begin
		
			select @sql = 'declare @r bigint, @s bigint, @minId bigint, @maxId bigint, @step bigint, @ns bigint, @ncsId bigint; '
				+ 'select @rc = count(1), @minId = min(' + @p_pk_column_name + '), @maxId = max(' + @p_pk_column_name + ') '
				+ 'from ' + @p_table_name + ' where ' + @table_scope + '; '
				+ 'if @rc = 0 return; '
				+ 'select @s = @minId; '
				+ 'select @step = @maxId; '
				+ 'select @copied_startid = ident_current(''' + @p_table_name + ''') + 1; '
				+ 'select @ns = @copied_startid + @rc; '
				+ 'dbcc checkident (''' + @p_table_name + ''', reseed, @ns); '					
				+ 'select @ncsId = @copied_startid; '
				+ 'while @s <= @maxId begin '
				+ 'begin try set identity_insert ' + @p_table_name + ' on;end try begin catch end catch; '
				+ 'insert into ' + @p_table_name + ' (' + @p_table_columns + ') '
				+ 'select ' + replace(@table_columns, 'x.[' + @p_pk_column_name + ']', '@ncsId + (row_number() over (order by x.' + @p_pk_column_name + ')) - 1') + ' '
				+ 'from ' + @p_table_name + ' x ' 
				+ @sql_join + ' '
				+ 'where ' + @table_scope + ' '
				+ 'and x.' + @p_pk_column_name + ' between @s and @s + @step; '
				+ 'select @r = @@rowcount; '
				+ 'select @ncsId = @ncsId + @r; '
				+ 'select @s = @s + @step + 1; '
				+ 'set identity_insert ' + @p_table_name + ' off; '
				+ 'end;'
		end
		else
		begin
			select @sql = 'select @copied_startid = ident_current(''' + @p_table_name + ''') + 1; '
				+ 'insert into ' + @p_table_name + ' '
				+ 'select ' + @table_columns + ' from ' + @p_table_name + ' x ' 
				+ @sql_join 
				+ ' where ' + @table_scope + ';'
				+ ' select @rc = @@rowcount;'
		end
		print @sql
		exec sp_executesql @sql, N'@copied_startid bigint output, @rc bigint output', @copied_startid output, @rc output
		
		update dbo.sys_index_table_included_copy 
		set copied_startid = @copied_startid, copied_rows = @rc, copied_endtime = getdate() 
		where copy_batch_code = @p_copy_batchcode 
			and upper(fk_table_name) = upper(@p_table_name)
		
		fetch next from copy_cursor 
		into @p_table_name, @table_order, @table_scope, @table_columns
	end
	close copy_cursor
	deallocate copy_cursor
END
GO
/****** Object:  StoredProcedure [dbo].[usp_bulkloader]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_bulkloader]
	-- Add the parameters for the stored procedure here 
	@p_index_table_name		varchar(128),
	@p_index_loadfromid		bigint
WITH EXECUTE AS OWNER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	declare @p_index_column_name varchar(128)	
	
	select top 1 @p_index_column_name = column_name 
	from dbo.v_table_columns 
	where upper(table_name) = upper(@p_index_table_name) 
		and is_identity = 1

	if @p_index_column_name = null
		raiserror ('Sorry, table %1 has no single identity primary key.', 42301, 1, @p_index_table_name)	
	
	exec dbo.usp_index_table_included_scope_setup @p_index_table_name, @p_index_column_name
	
	select distinct fk_table_order, fk_table_name table_name, replace(fk_table_scope, '#ID#', cast(@p_index_loadfromid as varchar(50))) load_scope
	from dbo.sys_index_table_included_scope
	where upper(index_table_name) = upper(@p_index_table_name)
		and fk_table_order is not null 
	order by fk_table_order
END
GO
/****** Object:  StoredProcedure [dbo].[usp_index_table_included_scope_init]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		Fei Wang
-- Create date: 16/12/2013,
-- Description:	This recursive stored procedure is called by dbo.usp_index_table_included_scope_step, 
--				selecting the dbo.v_table_references view to find the table relationships and then 
--				setting the reference scope statements.
CREATE PROCEDURE [dbo].[usp_index_table_included_scope_init]
	@p_database_version		varchar(128),
	@p_index_table_name		varchar(128), 
	@p_index_column_name	varchar(128),
	@p_pk_table_name		varchar(128),
	@p_reference_depth		int
	WITH EXECUTE AS OWNER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON
	
	-- declare a cursor for getting the foreign key tables of the given primary key table from the dbo.sys_index_table_included_scope by given index table.
	declare @fk_table_name varchar(128)
	declare csr_scope cursor local read_only for 
	select distinct a.fk_table_name  
	from dbo.sys_index_table_included_scope a  
	where upper(a.index_table_name) = upper(@p_index_table_name) 
		and upper(a.pk_table_name) = upper(@p_pk_table_name)
	
	-- increase the reference depth.
	select @p_reference_depth = @p_reference_depth + 1
	
	-- select foreign key table information from dbo.v_table_references, set the reference scope statement and save them into the dbo.sys_index_table_included_scope.
	insert into dbo.sys_index_table_included_scope 
	select @p_database_version, @p_index_table_name, a.pk_table_name, a.pk_column_name, a.fk_table_name, a.fk_column_name, a.fk_column_ordinal, 
		a.fk_column_nullable, @p_reference_depth, 
		case when upper(a.pk_table_name) = upper(@p_index_table_name) and upper(a.pk_column_name) = upper(@p_index_column_name) then 
			a.fk_column_name + ' in (#ID#)'
		else
			a.fk_column_name + ' in (select ' + a.pk_column_name + ' from ' + a.pk_table_name + ' where ?' + upper(a.pk_table_name) + '?)' 
		end reference_scope, null fk_table_order, null fk_table_scope, null fk_table_columns 
	from dbo.v_table_references a
	where upper(a.pk_table_name) = upper(@p_pk_table_name) 
	order by a.fk_table_name, a.fk_column_ordinal
	
	-- open cursor, start recursion.
	open csr_scope
	fetch next from csr_scope 
	into @fk_table_name 
	while @@fetch_status = 0
	begin
		if not exists (
			select 1 
			from dbo.sys_index_table_included_scope 
			where upper(index_table_name) = upper(@p_index_table_name) 
				and upper(pk_table_name) = upper(@fk_table_name)
			)
		exec dbo.usp_index_table_included_scope_init @p_database_version, @p_index_table_name, @p_index_column_name, @fk_table_name, @p_reference_depth
		
		fetch next from csr_scope 
		into @fk_table_name
	end
	close csr_scope
	deallocate csr_scope
END
GO
/****** Object:  StoredProcedure [dbo].[usp_index_table_included_scope_process]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		Fei Wang
-- Create date: 16/12/2013,
-- Description:	This recursive stored procedure is called by dbo.usp_index_table_included_scop_setup,
--				using the dbo.uf_get_index_table_included_scope function to get the scoping statement for a table based on the reference scope, 
--				and then update the reference scope for the next table.
--				It also calculate the table orders and store them for the bulkloader, bulkcopier, bulkcleaner.
CREATE PROCEDURE [dbo].[usp_index_table_included_scope_process]
	-- Add the parameters for the stored procedure here 
	@p_index_table_name		varchar(128),
	@p_index_column_name	varchar(128),
	@p_table_name			varchar(128),
	@p_going_down			int = 1
WITH EXECUTE AS OWNER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON		
	
	declare @pk_table_name varchar(128)
	declare @fk_table_name varchar(128)
	declare @fk_table_order int
	declare @fk_table_scope varchar(max)
	
	-- declare a cursor for getting primary key tables of the given table from dbo.sys_index_table_included_scope by given index table.
	declare upref_cursor cursor local read_only for 
		select distinct a.pk_table_name  
		from dbo.sys_index_table_included_scope a 
		where upper(a.index_table_name) = upper(@p_index_table_name) 
			and upper(a.fk_table_name) = upper(@p_table_name) 
			and upper(a.pk_table_name) <> upper(a.fk_table_name) 
		order by a.pk_table_name 

	-- declare a cursor for getting foreign key tables of the giving table from dbo.sys_index_table_included_scope by given index table.	
	declare downref_cursor cursor local read_only for 
		select distinct a.fk_table_name 
		from dbo.sys_index_table_included_scope a 
		where upper(a.index_table_name) = upper(@p_index_table_name) 
			and upper(a.pk_table_name) = upper(@p_table_name) 
			and upper(a.fk_table_name) <> upper(a.pk_table_name)
		order by a.fk_table_name
	
	-- open upref_cursor, process all unprocessed primary key tables with out start recursion.
	open upref_cursor
	fetch next from upref_cursor 
	into @pk_table_name
	while @@fetch_status = 0
	begin
		if not exists (select 1 from sys_index_table_included_scope where upper(index_table_name) = upper(@p_index_table_name) and upper(fk_table_name) = upper(@pk_table_name) and fk_table_scope is not null)
			exec dbo.usp_index_table_included_scope_process @p_index_table_name, @p_index_column_name, @pk_table_name, 0
			
		fetch next from upref_cursor 
		into @pk_table_name
	end
	close upref_cursor
	deallocate upref_cursor
	
	-- if the given table has not been processed, then process it.
	if not exists (select 1 from sys_index_table_included_scope where upper(index_table_name) = upper(@p_index_table_name) and upper(fk_table_name) = upper(@p_table_name) and fk_table_scope is not null) 
	begin
		-- get the last table order.
		select @fk_table_order = isnull(max(fk_table_order), 0)
		from sys_index_table_included_scope 
		where upper(index_table_name) = upper(@p_index_table_name)
			and fk_table_scope is not null
		
		-- increase the table order, get the table scoping statement, and update them for given table in dbo.sys_index_table_table_included_scope by given index table.
		select @fk_table_order = @fk_table_order + 1
		select @fk_table_scope = dbo.uf_get_index_table_included_scope(@p_index_table_name, @p_index_column_name, @p_table_name)
		update dbo.sys_index_table_included_scope
		set fk_table_order = @fk_table_order, fk_table_scope = @fk_table_scope 
		where upper(index_table_name) = upper(@p_index_table_name) 
			and upper(fk_table_name) = upper(@p_table_name)

		-- update all reference scope statements that involve this given table in dbo.sys_index_table_included_scope by given index table.
		update dbo.sys_index_table_included_scope 
		set reference_scope = replace(reference_scope, '?' + upper(@p_table_name) + '?', '(' + @fk_table_scope + ')')
		where upper(index_table_name) = upper(@p_index_table_name)
	end
	
	-- if needs to start recursion, open cursor and start recursion.
	if @p_going_down = 1
	begin
		open downref_cursor
		fetch next from downref_cursor 
		into @fk_table_name
		while @@fetch_status = 0 
		begin 
			execute dbo.usp_index_table_included_scope_process @p_index_table_name, @p_index_column_name, @fk_table_name, 1
			
			fetch next from downref_cursor 
			into @fk_table_name
		end
		close downref_cursor
		deallocate downref_cursor
	end

END
GO
/****** Object:  StoredProcedure [dbo].[usp_index_table_included_scope_setup]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		Fei Wang
-- Create date: 16/12/2013,
-- Description:	Main stored procedure for generating index table included meta data into dbo.sys_index_table_included_scope.
--				The meta data includes all relationships between tables that reference to the index table directly or indirectly, 
--				besides the relationships, it has scoping statements and column list of each table which can be used by bulkloader, bulkcopier and bulkcleaner.
CREATE PROCEDURE [dbo].[usp_index_table_included_scope_setup]
	@p_index_table_name		varchar(128), 
	@p_index_column_name	varchar(128)
WITH EXECUTE AS OWNER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	-- get the database version no. if there is no database version, raise an error.
	declare @datebase_version varchar(50)
	select top 1 @datebase_version = version_no 
	from database_version
	if @datebase_version is null 
		raiserror ('Sorry, database version is not set.', 42302, 1, @p_index_column_name)
    
    -- if the meta data for the index table and the latest database version has already there, return.
	if exists (select 1 from dbo.sys_index_table_included_scope where upper(database_version) = upper(@datebase_version) and upper(index_table_name) = upper(@p_index_table_name)) 
		return
	
	begin transaction

	-- delete the old meta data.
	delete from dbo.sys_index_table_included_scope 
	where upper(index_table_name) = upper(@p_index_table_name)
	
	-- insert the index table meta data.
	insert into dbo.sys_index_table_included_scope values (
		@datebase_version, @p_index_table_name, @p_index_table_name, @p_index_column_name, @p_index_table_name, @p_index_column_name, 1, 0, 0, 
		@p_index_column_name + ' in (#ID#)', 0, @p_index_column_name + ' in (#ID#)', null)
	
	-- step 1, call the dbo.usp_index_table_included_scope_init stored procedure to generate the table relationships.
	exec dbo.usp_index_table_included_scope_init @datebase_version, @p_index_table_name, @p_index_column_name, @p_index_table_name, 0
	
	-- step 2, call the dbo.usp_index_table_included_scope_process stored procedure to generate the scoping statements.
	exec dbo.usp_index_table_included_scope_process @p_index_table_name, @p_index_column_name, @p_index_table_name	

	-- step 3, get the tables column lists.
	update dbo.sys_index_table_included_scope
	set fk_table_columns = (select 'x.[' + column_name + '],' 
		 from dbo.v_table_columns  
		 where table_name = dbo.sys_index_table_included_scope.fk_table_name 
			--and is_identity = 0
		 order by ordinal_position 
		 for xml path(''), type)
		 .value('.', 'VARCHAR(max)')
	where upper(index_table_name) = upper(@p_index_table_name)
	
	update dbo.sys_index_table_included_scope 
	set fk_table_columns = substring(fk_table_columns, 1, len(fk_table_columns) - 1)
	where upper(index_table_name) = upper(@p_index_table_name) 
		and fk_table_columns is not null
	
	commit transaction
END
GO
/****** Object:  StoredProcedure [dbo].[usp_scenario_copy]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_scenario_copy]
	@p_copy_batchcode			varchar(50),
	@p_from_scenario_id			bigint,
	@p_to_scenario_id			bigint,
	@p_excluded_tablenames		varchar(max) = ''
WITH EXECUTE AS OWNER 
AS
BEGIN
	SET NOCOUNT ON
	
	declare @p_copied_startid bigint

	-- general copy.
	select @p_excluded_tablenames = @p_excluded_tablenames + ',integrated_scenario_backup,user_sychronize_record'
	exec [dbo].[usp_bulkcopier] 'integrated_scenario', @p_from_scenario_id, @p_to_scenario_id, @p_copy_batchcode, @p_excluded_tablenames
	
	create table #t_id_mapping 
	(
		id bigint primary key,
		new_id bigint
	)
	
	-- specific adjust.
	select top 1 @p_copied_startid = copied_startid 
	from dbo.sys_index_table_included_copy 
	where copy_batch_code = @p_copy_batchcode 
		and fk_table_name = 'job_type'
	
	insert into #t_id_mapping 
	select id, @p_copied_startid + row_number() over (order by id) - 1 new_id
	from job_type 
	where integrated_scenario_id = @p_from_scenario_id
	
	-- Update job and barracks job's job type id.
	update a 
	set a.int_field1 = b.new_id 
	from activities a, #t_id_mapping b 
	where b.id = a.int_field1 
		and a.integrated_scenario_id = @p_to_scenario_id 
		and a.type_id in (10, 11)
	
	delete from #t_id_mapping
	select top 1 @p_copied_startid = copied_startid 
	from dbo.sys_index_table_included_copy 
	where copy_batch_code = @p_copy_batchcode 
		and fk_table_name = 'activities'
	
	insert into #t_id_mapping 
	select id, @p_copied_startid + row_number() over (order by id) - 1 new_id
	from activities 
	where integrated_scenario_id = @p_from_scenario_id	
		
	-- Update parent and source activity's id.
	update a 
	set a.parent_id = b.new_id 
	from activities a, #t_id_mapping b 
	where b.id = a.parent_id 
		and a.integrated_scenario_id = @p_to_scenario_id
		
	update a 
	set a.source_activity_id = b.new_id 
	from activities a, #t_id_mapping b 
	where b.id = a.source_activity_id
		and a.integrated_scenario_id = @p_to_scenario_id
	
	-- Update related jobs' id of home pass jobs.
	update a 
	set a.int_field1 = b.new_id  
	from activities a, #t_id_mapping b 
	where b.id = a.int_field1 
		and a.integrated_scenario_id = @p_to_scenario_id 
		and a.type_id = 19
	
	delete from #t_id_mapping
	select top 1 @p_copied_startid = copied_startid 
	from dbo.sys_index_table_included_copy 
	where copy_batch_code = @p_copy_batchcode 
		and fk_table_name = 'training'
	
	insert into #t_id_mapping 
	select id, @p_copied_startid + row_number() over (order by id) - 1 new_id
	from training 
	where id < @p_copied_startid
	
	-- Update related training's id of training task.		
	update a 
	set a.int_field1 = b.new_id  
	from activities a, #t_id_mapping b 
	where b.id = a.int_field1 
		and a.integrated_scenario_id = @p_to_scenario_id 
		and a.type_id = 1
	
	delete from #t_id_mapping	
	select top 1 @p_copied_startid = copied_startid 
	from dbo.sys_index_table_included_copy 
	where copy_batch_code = @p_copy_batchcode 
		and fk_table_name = 'scenario_resource'
		
	insert into #t_id_mapping 
	select id, @p_copied_startid + row_number() over (order by id) - 1 new_id
	from scenario_resource 
	where integrated_scenario_id = @p_from_scenario_id 
		and resource_id in (
			select id 
			from resource 
			where resource_type in (1007101, 1007701)
			)
		
	-- Update trainconsist's id of home pass job.
	update a 
	set a.int_field3 = b.new_id 
	from activities a, #t_id_mapping b 
	where b.id = a.int_field3 
		and a.integrated_scenario_id = @p_to_scenario_id 
		and a.type_id = 19
	
	-- Update car's id of home pass job.		
	update a set a.int_field4 = b.new_id  
	from activities a, #t_id_mapping b 
	where b.id = a.int_field4 
		and a.integrated_scenario_id = @p_to_scenario_id 
		and a.type_id = 19
			
	delete from #t_id_mapping	
	select top 1 @p_copied_startid = copied_startid 
	from dbo.sys_index_table_included_copy 
	where copy_batch_code = @p_copy_batchcode 
		and fk_table_name = 'crew_role'
	
	insert into #t_id_mapping 
	select id, @p_copied_startid + row_number() over (order by id) - 1 new_id 
	from crew_role 
	where integrated_scenario_id = @p_from_scenario_id
		
	-- Update planning equivalent's id of crew role.
	update a set a.planning_equivalent = b.new_id 
	from crew_role a, #t_id_mapping b 
	where b.id = a.planning_equivalent 
		and a.integrated_scenario_id = @p_to_scenario_id
	
	-- Update next level's id of crew role.	
	update a set a.next_level = b.new_id 
	from crew_role a, #t_id_mapping b 
	where b.id = a.next_level 
		and a.integrated_scenario_id = @p_to_scenario_id
		
	-- Update capacity sub type's id of route_crew_capacity.
	update a set a.capacity_subtype = b.new_id
	from route_crew_capacity a, #t_id_mapping b 
	where b.id = a.capacity_subtype 
		and a.integrated_scenario_id = (select top 1 id from scenario where integrated_scenario_id = @p_to_scenario_id)
		
	-- Update capacity sub type's id of route_crew_availability.
	update a set a.capacity_subtype = b.new_id
	from route_crew_availability a, #t_id_mapping b 
	where b.id = a.capacity_subtype 
		and a.integrated_scenario_id = (select top 1 id from scenario where integrated_scenario_id = @p_to_scenario_id)

	-- Update primary_qual_id's id of resource_qualification.
	select top 1 @p_copied_startid = copied_startid 
	from dbo.sys_index_table_included_copy 
	where copy_batch_code = @p_copy_batchcode 
		and fk_table_name = 'resource_qualification'
	
	delete from #t_id_mapping
	insert into #t_id_mapping 
	select id, @p_copied_startid + row_number() over (order by id) - 1 new_id
	from resource_qualification 
	where integrated_scenario_id = @p_from_scenario_id

	update a set a.primary_qual_id = b.new_id
	from resource_qualification a, #t_id_mapping b 
	where b.id = a.primary_qual_id 
		and a.integrated_scenario_id = @p_to_scenario_id

		
	drop table #t_id_mapping
	
	alter index ix_integrated_scenario_id on activities rebuild
	alter index ix_integrated_scenario_id on perform_activity rebuild
	alter index ix_integrated_scenario_id on resource rebuild
	alter index ix_integrated_scenario_id on scenario_resource rebuild
	alter index ix_integrated_scenario_id on scenario_resource_association rebuild
	alter index ix_integrated_scenario_id on scenario_resource_activity_join_table rebuild
	alter index ix_integrated_scenario_id on resource_qualification rebuild
	alter index ix_integrated_scenario_id on employee_qualification rebuild
END
GO
/****** Object:  StoredProcedure [dbo].[usp_scenario_delete]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_scenario_delete]
	@p_clean_batchcode			varchar(50),
	@p_scenario_id				bigint,
	@p_clean_to_index_table		int = 1
WITH EXECUTE AS OWNER 
AS
BEGIN
	SET NOCOUNT ON

	-- mark integrated_scenario 'to be delete'.
	update integrated_scenario set batchcode=@p_clean_batchcode, to_delete=1 where id=@p_scenario_id and live = 0
	
	-- check and fix the lastopenedscenario configuration in user_parameters.
	update a 
	set a.parameter_value = cast((select min(id) from integrated_scenario where live = 1) as varchar(max)) 
	from user_parameters a 
	where not exists (
		select id 
		from integrated_scenario 
		where id = cast(a.parameter_value as int)
		) and parameter_code = 'LastOpenedScenario'
		
END
GO
/****** Object:  StoredProcedure [dbo].[usp_scenario_load]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_scenario_load] 
	-- Add the parameters for the stored procedure here
	@p_scenario_id				bigint, 
	@p_excluded_tablenames		varchar(max) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON

	declare @excluded_tablenames_x varchar(max)
	declare @table_load_order int
	declare @table_name varchar(150)
	declare @table_load_scope varchar(max)
	declare @excluded_tablename varchar(150)
	
	declare @table_pk_columnname varchar(150)
	declare @min_pk_columnvalue bigint
	declare @max_pk_columnvalue bigint
	declare @sql nvarchar(max)
	
	select @excluded_tablenames_x = '(?' + upper(replace(@p_excluded_tablenames, ',', '?),(?')) + '?)'
	
	exec [dbo].[usp_bulkloader] 'integrated_scenario', @p_scenario_id
	
    declare load_cursor cursor local read_only for 
		select distinct fk_table_order, fk_table_name table_name, upper(replace(fk_table_scope, '#ID#', cast(@p_scenario_id as varchar(50)))) load_scope
		from dbo.sys_index_table_included_scope 
		where upper(index_table_name) = upper('integrated_scenario')
			and fk_table_order is not null 
		order by fk_table_order
		
	open load_cursor
	fetch next from load_cursor 
	into @table_load_order, @table_name, @table_load_scope
	while @@fetch_status = 0
	begin
		while PATINDEX('%(?%?)%', @table_load_scope) > 0
		begin
			select @excluded_tablename = substring(@table_load_scope, PATINDEX('%(?%?)%', @table_load_scope), PATINDEX('%?)%', @table_load_scope) - PATINDEX('%(?%?)%', @table_load_scope) + 2)
			if charindex(@excluded_tablename, @excluded_tablenames_x) > 0 
				select @table_load_scope = replace(@table_load_scope, @excluded_tablename, '(0 = 1)')
			else
				select @table_load_scope = replace(@table_load_scope, @excluded_tablename + ' and', '')
		end
		
		declare @r_table_load_scope nvarchar(max) 
		select @r_table_load_scope = @table_load_scope
		if upper(@table_name) <> upper('integrated_scenario')

			exec dbo.usp_optimise_table_scope @table_name, @table_load_scope, @r_table_load_scope output 
			select @table_load_scope = 'select * from ' + @table_name + ' where ' + @r_table_load_scope 
		
		
		select upper(@table_load_scope) table_query
		fetch next from load_cursor  
		into @table_load_order, @table_name, @table_load_scope
	end
	close load_cursor
	deallocate load_cursor
END
GO
/****** Object:  StoredProcedure [dbo].[usp_trip_delete]    Script Date: 30/05/2016 4:47:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_trip_delete]
	@p_user_name				varchar(50),
	@p_iscenario_id				bigint, 
	@p_trip_codes				varchar(max),
	@p_delete_anyway			int = 0,
	@p_usync_record_id			bigint
WITH EXECUTE AS OWNER 
AS
BEGIN
	SET NOCOUNT ON;	

	declare @p_rows_scope varchar(max);
	
	declare @TRIP_TYPE int, @JOB_TYPE int, @PERFORMED_CREWTASK_TYPE int, @SIGNON_CREWTASK_TYPE int, @TRAVELTOFROM_CREWTASK_TYPE int, @SIGNOFF_CREWTASK_TYPE int, 
		@IDLE_CREWTASK_TYPE int, @HANDOVER_CREWTASK_TYPE int, @TRAINSTARTUP_CREWTASK_TYPE int, @TRAINSHUTDOWN_CREWTASK_TYPE int;
	
	declare @sql varchar(max);
	
	select 0 id, cast(0 as varchar(max)) code 
	into #trips 
	from activities 
	where id = 0;
	
	select 0 id 
	into #activities 
	from activities 
	where id = 0;
	
	declare @trip_id int;
	declare @trip_code varchar(max);
	declare @validated bit;
	declare trip_cursor cursor local read_only for 
		select id, code 
		from #trips;
	
	select @TRIP_TYPE = id 
	from activity_type 
	where owner = 'com.solveit.rail.scheduling.trip.Trip';
	select @JOB_TYPE = id 
	from activity_type 
	where owner = 'com.solveit.rostering.program.Job';
	select @PERFORMED_CREWTASK_TYPE = id 
	from activity_type 
	where owner = 'com.solveit.rostering.program.task.PerformActivityTask';
	select @SIGNON_CREWTASK_TYPE = id 
	from activity_type 
	where owner = 'com.solveit.rostering.program.task.SignOnTask';
	select @TRAVELTOFROM_CREWTASK_TYPE = id 
	from activity_type 
	where owner = 'com.solveit.rostering.program.task.TravelToAndFromWorkTask';
	select @SIGNOFF_CREWTASK_TYPE = id 
	from activity_type 
	where owner = 'com.solveit.rostering.program.task.SignOffTask';
	select @IDLE_CREWTASK_TYPE = id
	from activity_type 
	where owner = 'com.solveit.rostering.program.task.IdleTask';
	select @HANDOVER_CREWTASK_TYPE = id
	from activity_type 
	where owner = 'com.solveit.rostering.program.task.HandoverTask';
	select @TRAINSTARTUP_CREWTASK_TYPE = id
	from activity_type 
	where owner = 'com.solveit.rostering.program.task.TrainStartupTask';
	select @TRAINSHUTDOWN_CREWTASK_TYPE = id
	from activity_type 
	where owner = 'com.solveit.rostering.program.task.TrainShutdownTask';
	
	select @sql = 'insert into #trips select id, string_field1 code from activities where scenario_id = ' 
		+ cast(@p_iscenario_id as varchar(50)) 
		+ ' and type_id = ' + cast(@TRIP_TYPE as varchar(50)) + ' and string_field1 in (' 
		+ @p_trip_codes + ')';
	execute(@sql);
	
	open trip_cursor;
	fetch next from trip_cursor 
	into @trip_id, @trip_code
	while @@fetch_status = 0
	begin	
		select @validated = 1;
		
		select id 
		into #triplegs 
		from activities 
		where parent_id = @trip_id;
			
		select id, period_id 
		into #railtasks 
		from activities 
		where parent_id in (
			select id 
			from #triplegs
			);
		
		select crew_task_id id, performed_activity_id railtask_id
		into #crewtasks 
		from perform_activity 
		where performed_activity_id in (
			select id 
			from #railtasks
			);
		
		select distinct x.parent_id id
		into #jobs 
		from activities x
		where x.id in (
			select id 
			from #crewtasks
			);
						
		-- add handover task if there is.
		insert into #crewtasks 
		select m.id, n.id
		from (
			select a.id, b.start_date, b.end_date 
			from activities a, time_periods b 
			where a.parent_id in (
				select id 
				from #jobs
				) and a.type_id = @HANDOVER_CREWTASK_TYPE
			) m, (
			select a.id, b.start_date, b.end_date
			from #railtasks a, time_periods b 
			where a.id not in (
				select railtask_id 
				from #crewtasks
				) and b.id = a.period_id
			) n
		where n.start_date = m.start_date
			and n.end_date = m.end_date;
			
		select distinct barracks_job_id id
		into #barracksjobs 
		from barracks_to_jobs 
		where previous_job_id in (
			select id 
			from #jobs
			);
			
		select id 
		into #selfdrivejobs 
		from activities 
		where parent_id in (
			select id 
			from #barracksjobs
			);
			
		select id 
		into #r_selfdrivejobs 
		from activities 
		where parent_id in (
			select id 
			from #selfdrivejobs
			);
		
		select id 
		into #homepassjobs 
		from activities 
		where int_field1 in (
			select id 
			from #jobs
			);
		
		select id 
		into #homepassdrivejobs 
		from activities 
		where parent_id in (
			select id 
			from #homepassjobs
			);
			
		-- do validation.
		if @p_delete_anyway = 0
		begin
			if exists (
				select 1 from employee_tracking 
				where planned_allocation_id in (
					select id 
					from employee_allocation 
					where job_id in (
						select id 
						from #jobs 
						union 
						select id 
						from #r_selfdrivejobs
						union
						select id 
						from #homepassdrivejobs
						)
					)
				) 
			begin
				-- has actual records against jobs. can not be deleted.
				select @validated = 0;
			end;
		end;
		
		if @validated = 0
		begin
			-- insert an exception log.
			insert into synchronization_record 
			values (0, 0, 0, 'ImportSyncTrainTrip', 'Trip ' + @trip_code + ' has actual records against its jobs, so it can not be deleted.', 2, 4, getdate(), @p_user_name, @p_usync_record_id, null, null, null, null);
		end;
		else
		begin				
			insert into #activities 
			select @trip_id 
			union 
			select id from #triplegs 
			union 
			select id from #railtasks 
			union 
			select id from #crewtasks
			
			insert into synchronization_record 
			values (0, 0, 0, 'ImportSyncTrainTrip', 'Trip ' + @trip_code + ' has been deleted.', 117, 1, getdate(), @p_user_name, @p_usync_record_id, null, null, null, null);
		end;
		
		drop table #triplegs;
		drop table #railtasks;
		drop table #crewtasks;
		drop table #jobs;
		drop table #barracksjobs;
		drop table #selfdrivejobs;
		drop table #r_selfdrivejobs;
		drop table #homepassjobs;
		drop table #homepassdrivejobs;
		
		fetch next from trip_cursor 
		into @trip_id, @trip_code;		
	end;
	close trip_cursor;
	deallocate trip_cursor;
		
	drop table #trips;
	
	select distinct x.parent_id id
	into #all_jobs 
	from activities x
	where x.id in (
		select id 
		from #activities
		) and 
		x.type_id in (@PERFORMED_CREWTASK_TYPE, @HANDOVER_CREWTASK_TYPE);	
	
	select distinct barracks_job_id id
	into #all_barracksjobs 
	from barracks_to_jobs 
	where previous_job_id in (
		select id 
		from #all_jobs
		);
		
	select id 
	into #all_selfdrivejobs 
	from activities 
	where parent_id in (
		select id 
		from #all_barracksjobs
		);
		
	select id 
	into #all_r_selfdrivejobs 
	from activities 
	where parent_id in (
		select id 
		from #all_selfdrivejobs
		);
	
	select id 
	into #all_homepassjobs 
	from activities 
	where int_field1 in (
		select id 
		from #all_jobs
		);
	
	select id 
	into #all_homepassdrivejobs 
	from activities 
	where parent_id in (
		select id 
		from #all_homepassjobs
		);
		
	insert into #activities 
	select id from #all_jobs 
	union 
	select id from #all_barracksjobs 
	union 
	select id from #all_selfdrivejobs 
	union 
	select id from #all_r_selfdrivejobs
	union 
	select id from #all_homepassjobs
	union 
	select id from #all_homepassdrivejobs;
				
	-- get non-performed-activity crew tasks(signon, travelto, travelfrom, signoff, startup, shutdown).
	insert into #activities 
	select id 
	from activities 
	where parent_id in (
		select id 
		from #all_jobs 
		union 
		select id 
		from #all_r_selfdrivejobs 
		union
		select id
		from #all_homepassdrivejobs
		) and type_id in (@SIGNON_CREWTASK_TYPE, @TRAVELTOFROM_CREWTASK_TYPE, @TRAINSTARTUP_CREWTASK_TYPE, @TRAINSHUTDOWN_CREWTASK_TYPE, @SIGNOFF_CREWTASK_TYPE, @IDLE_CREWTASK_TYPE);
	
	select @p_rows_scope = 'select id from #activities';
	
	-- clear barracks_allocations with barracks job id in the deleted jobs.
	delete from barracks_allocation 
	where barracks_job_id in (
		select barracks_job_id 
		from barracks_to_jobs 
		where previous_job_id in (
			select id 
			from #all_jobs
			)
		);

	-- clear barracks_to_job's with previous job id in the deleted jobs.
	delete from barracks_to_jobs 
	where previous_job_id in (
		select id 
		from #all_jobs
		);
	
	-- clear barrack_to_job's next job id.
	update barracks_to_jobs 
	set next_job_id = null 
	where next_job_id in (
		select id 
		from #all_jobs
		); 

	-- general clean.
	exec [dbo].[usp_bulkcleaner] 'activities', @p_rows_scope, @p_user_name, 'barracks_to_jobs', 1;
	
	drop table #all_jobs;
	drop table #all_barracksjobs;
	drop table #all_selfdrivejobs;
	drop table #all_r_selfdrivejobs;
	drop table #all_homepassjobs;
	drop table #all_homepassdrivejobs;
	drop table #activities;
END



GO
